class RenameResetDigesColumnToUsers < ActiveRecord::Migration[5.1]
  def change
    rename_column :users, :reset_diges, :reset_digest
  end
end
